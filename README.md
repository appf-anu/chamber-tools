# chamber-tools

* ![master:go-lint](https://gitlab.com/appf-anu/chamber-tools/badges/master/pipeline.svg?job=golint) *master:go-lint*

* ![master:go-staticcheck](https://gitlab.com/appf-anu/chamber-tools/badges/master/pipeline.svg?job=staticcheck) *master:go-staticcheck*

* ![master:go_vet](https://gitlab.com/appf-anu/chamber-tools/badges/master/pipeline.svg?job=go_vet) *master:go-vet*

* ![master:go_test](https://gitlab.com/appf-anu/chamber-tools/badges/master/pipeline.svg?job=go_test) *master:go-test*

* ![master:go_build](https://gitlab.com/appf-anu/chamber-tools/badges/master/pipeline.svg?job=go_build) *master:go-build*


tools for go controller-* applications

see http://gitlab.org/appf-anu/controller-heliospectra2

