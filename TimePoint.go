package chambertools

import (
	"fmt"
	"strings"
	"time"
)

/*
"psi_coolwhite",  # "channel-1",  # white
"psi_blue",  # "channel-2",  # blue
"psi_green",  # "channel-3",  # green
"psi_red",  # "channel-4",  # red
"psi_deepred",  # "channel-5",  #  deepred
"psi_farred",  # "channel-6",  # far red
"psi_infrared",  # "channel-7",  # infrared
"psi_cyan",  # "channel-8"   # NA
*/

// TimePoint type representation of a TimePoint in a control file (a row in xlsx sheet)
type TimePoint struct {
	Datetime         time.Time `header:"datetime"`
	SimDatetime      time.Time `header:"datetime-sim"`
	Temperature      float64   `header:"temperature"`
	RelativeHumidity float64   `header:"humidity"`
	Light1           int       `header:"light1"`
	Light2           int       `header:"light2"`
	CO2              float64   `header:"co2"`
	TotalSolar       float64   `header:"totalsolar"`
	CoolWhite        float64   `header:"coolwhite"`
	Blue             float64   `header:"blue"`
	Green            float64   `header:"green"`
	Red              float64   `header:"red"`
	DeepRed          float64   `header:"deepred"`
	FarRed           float64   `header:"farred"`
	InfraRed         float64   `header:"infrared"`
	RoyalBlue        float64   `header:"royalblue"`
	Cyan             float64   `header:"cyan"`
	Wavelength370nm  float64   `header:"370nm"`
	Wavelength380nm  float64   `header:"380nm"`
	Wavelength400nm  float64   `header:"400nm"`
	Wavelength420nm  float64   `header:"420nm"`
	Wavelength450nm  float64   `header:"450nm"`
	Wavelength530nm  float64   `header:"530nm"`
	Wavelength620nm  float64   `header:"620nm"`
	Wavelength630nm  float64   `header:"630nm"`
	Wavelength660nm  float64   `header:"660nm"`
	Wavelength735nm  float64   `header:"735nm"`
	Wavelength850nm  float64   `header:"850nm"`
	Wavelength5700k  float64   `header:"5700k"`
	Wavelength6500k  float64   `header:"6500k"`
}

// NewTimePointPtr creates a new TimePoint and returns the pointer to it
func NewTimePointPtr() *TimePoint {
	tp := &TimePoint{
		time.Time{},
		time.Time{},
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetInt,
		NullTargetInt,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
		NullTargetFloat64,
	}
	return tp
}

// NulledString replaces instances of nulltargets with the string "NULL"
func (tp TimePoint) NulledString() string {
	repr := fmt.Sprintf("%+v", tp)
	nullTargetFloatStr := fmt.Sprintf("%v", NullTargetFloat64)
	repr = strings.Replace(repr, nullTargetFloatStr, "NULL", -1)
	nullTargetIntStr := fmt.Sprintf("%v", NullTargetInt)
	repr = strings.Replace(repr, nullTargetIntStr, "NULL", -1)
	return repr
}
