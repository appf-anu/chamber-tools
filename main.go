package chambertools

import (
	"fmt"
	"github.com/bcampbell/fuzzytime"
	"github.com/mdaffin/go-telegraf"
	"github.com/tealeg/xlsx"
	"log"
	"math"
	"os"
	"path/filepath"
	"reflect"
	"time"
)

// it is extremely unlikely (see. impossible) that we will be measuring or sending a humidity of 214,748,365 %RH or
// a temperature of -340,282,346,638,528,859,811,704,183,484,516,925,440°C until we invent some new physics, so
// until then, I will use these values as the unset or null values for HumidityTarget and TemperatureTarget

// NullTargetInt exported see above
const NullTargetInt int = math.MinInt32

// NullTargetInt32 exported see above
const NullTargetInt32 int32 = math.MinInt32

// NullTargetInt64 exported see above
const NullTargetInt64 int64 = math.MinInt64

// NullTargetFloat64 exported see above
const NullTargetFloat64 float64 = -math.MaxFloat32

type stopIteration struct{}

func (m *stopIteration) Error() string {
	return "stopIteration"
}

var (
	ctx fuzzytime.Context
	// ZoneName exported so that packages that use this package can refer to the current timezone
	ZoneName   string
)


func init() {
	ZoneName, _ = time.Now().Zone()
	ctx = fuzzytime.Context{
		DateResolver: fuzzytime.DMYResolver,
		TZResolver:   fuzzytime.DefaultTZResolver(ZoneName),
	}
}

// Min returns a value clamped to a lower limit limit
func Min(value, limit int) int {
	if value < limit {
		return value
	}
	return limit
}

// Max returns a value clamped to an upper limit
func Max(value, limit int) int {
	if value > limit {
		return value
	}
	return limit
}

// Clamp clamps a value to between a minimum and maximum value
func Clamp(value, minimum, maximum int) int {
	return Min(Max(value, minimum), maximum)
}

// TelegrafTags convenient struct to store tags in
type TelegrafTags struct {
	Host   string
	Group  string
	DataID string
}

// WriteMetricUDP writes a metric a udp telegraf host based on environment variables.
func WriteMetricUDP(metricName string, tags TelegrafTags, thing interface{}) error {
	telegrafHost := "telegraf:8092"
	if tthost := os.Getenv("TELEGRAF_HOST"); tthost != "" {
		telegrafHost = tthost
	}

	telegrafClient, err := telegraf.NewUDP(telegrafHost)
	if err != nil {
		return err
	}
	defer telegrafClient.Close()

	m := telegraf.NewMeasurement(metricName)
	//va := reflect.ValueOf(&thing).Elem()
	va := reflect.ValueOf(thing)
	if va.Kind() == reflect.Ptr {
		va = reflect.Indirect(va)
	}
	if va.Kind() != reflect.Struct {
		panic("non-struct passed to WriteMetricTCP")
	}

	for i := 0; i < va.NumField(); i++ {
		DecodeStructFieldToMeasurement(&m, va, i)
	}

	if tags.Host != "" {
		m.AddTag("host", tags.Host)
	}
	if tags.Group != "" {
		m.AddTag("group", tags.Group)
	}
	if tags.DataID != "" {
		m.AddTag("did", tags.DataID)
	}

	return telegrafClient.Write(m)
}

// WriteMetricTCP writes a metric a tcp telegraf host based on environment variables.
func WriteMetricTCP(metricName string, tags TelegrafTags, thing interface{}) error {
	telegrafHost := "telegraf:8192"
	if tthost := os.Getenv("TELEGRAF_HOST"); tthost != "" {
		telegrafHost = tthost
	}

	telegrafClient, err := telegraf.NewTCP(telegrafHost)
	if err != nil {
		return err
	}
	defer telegrafClient.Close()

	m := telegraf.NewMeasurement(metricName)
	//va := reflect.ValueOf(&thing).Elem()
	va := reflect.ValueOf(thing)
	if va.Kind() == reflect.Ptr {
		va = reflect.Indirect(va)
	}
	if va.Kind() != reflect.Struct {
		panic("non-struct passed to WriteMetricTCP")
	}

	for i := 0; i < va.NumField(); i++ {
		DecodeStructFieldToMeasurement(&m, va, i)
	}

	if tags.Host != "" {
		m.AddTag("host", tags.Host)
	}
	if tags.Group != "" {
		m.AddTag("group", tags.Group)
	}
	if tags.DataID != "" {
		m.AddTag("did", tags.DataID)
	}

	return telegrafClient.Write(m)
}

// DecodeStructToLineProtocol encodes a simple struct to an influxdb line protocol representation
func DecodeStructToLineProtocol(metricName string, tags TelegrafTags, thing interface{}) string {
	m := telegraf.NewMeasurement(metricName)

	va := reflect.ValueOf(thing)
	if va.Kind() == reflect.Ptr {
		va = reflect.Indirect(va)
	}
	if va.Kind() != reflect.Struct {
		panic("non-struct passed to DecodeStructToLineProtocol")
	}
	for i := 0; i < va.NumField(); i++ {
		DecodeStructFieldToMeasurement(&m, va, i)
	}

	if tags.Host != "" {
		m.AddTag("host", tags.Host)
	}
	if tags.Group != "" {
		m.AddTag("group", tags.Group)
	}
	if tags.DataID != "" {
		m.AddTag("did", tags.DataID)
	}

	return m.ToLineProtocal()
}

// DecodeStructFieldToMeasurement turns a field of a struct into a measurement field and adds it to the measurment.
// doesnt support nested structs or maps, yet
func DecodeStructFieldToMeasurement(m *telegraf.Measurement, va reflect.Value, i int) {
	f := va.Field(i)
	fi := f.Interface()
	n := va.Type().Field(i).Name

	switch v := fi.(type) {
	case int64:
		if v == NullTargetInt64 {
			break
		}
		m.AddInt64(n, v)
	case int32:
		m.AddInt32(n, v)
	case int:
		if v == NullTargetInt {
			break
		}
		m.AddInt(n, v)
	case float64:
		if v == NullTargetFloat64 {
			break
		}
		m.AddFloat64(n, v)
	case string:
		m.AddString(n, v)
	case bool:
		m.AddBool(n, v)
	case []int64:
		for i, iv := range v {
			if iv == NullTargetInt64 {
				continue
			}
			m.AddInt64(fmt.Sprintf("%s_%02d", n, i+1), iv)
		}
	case []int32:
		for i, iv := range v {
			if iv == NullTargetInt32 {
				continue
			}
			m.AddInt32(fmt.Sprintf("%s_%02d", n, i+1), iv)
		}
	case []int:
		for i, iv := range v {
			if iv == NullTargetInt {
				continue
			}
			m.AddInt(fmt.Sprintf("%s_%02d", n, i+1), iv)
		}
	case []float64:
		for i, iv := range v {
			if iv == NullTargetFloat64 {
				continue
			}
			m.AddFloat64(fmt.Sprintf("%s_%02d", n, i+1), iv)
		}
	case []string:
		for i, iv := range v {
			if iv == "" {
				continue
			}
			m.AddString(fmt.Sprintf("%s_%02d", n, i+1), iv)
		}
	case []bool:
		for i, iv := range v {
			m.AddBool(fmt.Sprintf("%s_%02d", n, i+1), iv)
		}
	}
}


func newTimePointFromRow(errLog *log.Logger, row *xlsx.Row, headers *xlsx.Row) (*TimePoint, error) {
	tp := NewTimePointPtr()
	va := reflect.ValueOf(tp)
	if va.Kind() == reflect.Ptr {
		va = reflect.Indirect(va)
	}
	t := va.Type()
	cellIndex := 0
	fatalError := row.ForEachCell(func(cell *xlsx.Cell) error {
		defer func() { cellIndex++ }()
		thisHeader := headers.GetCell(cellIndex).String()

		for i := 0; i < va.NumField(); i++ {
			ft := t.Field(i)
			fv := va.Field(i)
			headerTag, ok := ft.Tag.Lookup("header")
			// skip fields with no header tag
			if !ok {
				continue
			}
			if thisHeader == headerTag {
				fvi := fv.Interface()
				switch theType := fvi.(type) {
				case time.Time:
					if cell.String() == ""{
						//errLog.Printf("Empty date for field \"%s\" row #%05d", headerTag, row.GetCoordinate()+1)
						return fmt.Errorf("empty date for field \"%s\" row #%05d", headerTag, row.GetCoordinate()+1)
					}
					value, err := cell.GetTime(false)

					if err != nil {
						return nil
					}
					fv.Set(reflect.ValueOf(value))
				case int, int64:
					value, err := cell.Int64()
					if err != nil {
						errLog.Println(err)
						return nil
					}
					fv.SetInt(value)
				case float64:
					value, err := cell.Float()
					if err != nil {
						errLog.Println(err)

						return nil
					}
					fv.SetFloat(value)
				case string:
					value := cell.String()
					if value == "" {
						return nil
					}
					fv.SetString(value)
				case bool:
					value := cell.Bool()
					fv.SetBool(value)
				default:
					errLog.Printf("wrong type %+v? Something bad happened with header \"%s\" for row  #%05d\n",
						theType, headerTag, row.GetCoordinate()+1)
					return nil
				}
			}
		}
		return nil
	})
	// freak out if there is fatalError
	if fatalError != nil {
		return nil, fatalError
	}
	return tp, nil
}

func loopFromXlsx(errLog *log.Logger, runStuff func(point *TimePoint) bool, conditionsPath string) {

	xlFile, err := xlsx.OpenFile(conditionsPath)
	if err != nil {
		log.Fatal(err)
	}
	// excel files dont require closing?
	//defer file.Close()
	sheet, ok := xlFile.Sheet["timepoints"]
	if !ok {
		fmt.Println("no sheet named \"timepoints\" in xlsx file")
		os.Exit(3)
	}

	var lastTp *TimePoint
	var lastTpIdx int
	firstRun := true

	var firstTime time.Time
	data := make([]*TimePoint, 0)
	rowIndex := 0
	headerRow, err := sheet.Row(0)
	if err != nil {
		errLog.Println("Couldn't get header row.")
		errLog.Fatal(err)
	}
	sheet.ForEachRow(func(row *xlsx.Row) error {
		defer func() { rowIndex++ }()

		if rowIndex == 0 { // always ignore the first row
			return nil
		}
		temptp, err := newTimePointFromRow(errLog, row, headerRow)
		if err != nil {
			errLog.Print(err)
			errLog.Print("Ignoring row")
			return nil
		}
		tempTime := temptp.Datetime
		// convert time to local tz
		theTime := time.Date(
			tempTime.Year(),
			tempTime.Month(),
			tempTime.Day(),
			tempTime.Hour(),
			tempTime.Minute(),
			tempTime.Second(),
			tempTime.Nanosecond(),
			time.Local)
		if firstTime.Unix() <= 0 { // check to see if firsttime has been set
			firstTime = theTime
		}
		if theTime.After(firstTime.Add(time.Hour * 24)) {
			return &stopIteration{}
		}
		tp, err := newTimePointFromRow(errLog, row, headerRow)
		if err != nil {
			errLog.Printf(" Error adding timepoint to loop data: %v", err)
			errLog.Print("Ignoring row")
			return nil
		}
		data = append(data, tp)
		return nil
	})

	totalTimepoints := len(data) - 1
	errLog.Printf("looping over %d timepoints", totalTimepoints)
	for {
		for i, tp := range data {
			thisTpIdx := i
			now := time.Now()

			theTime := time.Date(
				now.Year(),
				now.Month(),
				now.Day(),
				tp.Datetime.Hour(),
				tp.Datetime.Minute(),
				tp.Datetime.Second(),
				tp.Datetime.Nanosecond(),
				time.Local)

			if i == len(data)-1 { // end of data, set the next time to tomorrow
				lastTp = tp
				lastTpIdx = i
				thisTpIdx = 0
				theTime = time.Date(
					now.Year(),
					now.Month(),
					now.Day(),
					data[0].Datetime.Hour(),
					data[0].Datetime.Minute(),
					data[0].Datetime.Second(),
					data[0].Datetime.Nanosecond(),
					time.Local).Add(time.Hour * 24)
				errLog.Printf("Reached end of data, looping from beginning. next TimePoint at %v ", theTime)
			} else {
				// check if theTime is Before
				if theTime.Before(time.Now()) {
					lastTp = tp
					lastTpIdx = i
					continue
				}
			}

			// run the last timepoint if its the first run.
			if firstRun {
				firstRun = false
				errLog.Printf("running initial TimePoint %05d/%05d", lastTpIdx, totalTimepoints)
				for i := 0; i < 10; i++ {
					errLog.Printf("TimePoint: %s", lastTp.NulledString())
					if runStuff(lastTp) {
						break
					}
				}
			}

			// we have reached sleeptime
			errLog.Printf("sleeping for %s until TimePoint %05d/%05d at %v",
				time.Until(theTime).String(), thisTpIdx, totalTimepoints, tp.Datetime)
			time.Sleep(time.Until(theTime))

			// RUN STUFF HERE
			for try := 0; try < 10; try++ {
				errLog.Printf("running TimePoint %05d/%05d", thisTpIdx, totalTimepoints)
				errLog.Printf("TimePoint: %s", tp.NulledString())
				if runStuff(tp) {
					break
				}
			}
			// end RUN STUFF
		}
	}
}

func runFromXlsx(errLog *log.Logger, runStuff func(point *TimePoint) bool, conditionsPath string) {
	xlFile, err := xlsx.OpenFile(conditionsPath)
	if err != nil {
		log.Fatal(err)
	}
	// excel files dont require closing?
	//defer file.Close()
	sheet, ok := xlFile.Sheet["timepoints"]
	if !ok {
		fmt.Println("no sheet named \"timepoints\" in xlsx file")
		os.Exit(3)
	}

	var lastRow *xlsx.Row
	firstRun := true
	rowIndex := 0

	headerRow, err := sheet.Row(0)
	if err != nil {
		errLog.Println("Couldn't get header row.")
		errLog.Fatal(err)
	}

	sheet.ForEachRow(func(row *xlsx.Row) error {
		defer func() { rowIndex++ }()
		if rowIndex == 0 {
			// skip the first row
			return nil
		}

		temptp, err := newTimePointFromRow(errLog, row, headerRow)
		if err != nil {
			errLog.Print(err)
			errLog.Print("Ignoring row")
			return nil
		}

		tempTime := temptp.Datetime
		theTime := time.Date(
			tempTime.Year(),
			tempTime.Month(),
			tempTime.Day(),
			tempTime.Hour(),
			tempTime.Minute(),
			tempTime.Second(),
			tempTime.Nanosecond(),
			time.Local)

		// if we are before the time skip until we are after it
		if theTime.Before(time.Now()) {
			lastRow = row
			return nil
		}

		// run the last timepoint if its the first run.
		if firstRun {
			firstRun = false
			errLog.Println("running firstrun line")
			for i := 0; i < 10; i++ {
				tp, err := newTimePointFromRow(errLog, lastRow, headerRow)
				if err != nil {
					errLog.Println(err)
					errLog.Print("Ignoring row")
					break
				}
				errLog.Printf("TimePoint: %s", tp.NulledString())
				if runStuff(tp) {
					break
				}
			}
		}

		// we have reached sleeptime
		errLog.Printf("sleeping for %s\n", time.Until(theTime).String())
		time.Sleep(time.Until(theTime))

		// RUN STUFF HERE
		for i := 0; i < 10; i++ {
			tp, err := newTimePointFromRow(errLog, row, headerRow)
			if err != nil {
				errLog.Println(err)
				errLog.Print("Ignoring row")
				break
			}
			errLog.Printf("TimePoint: %s", tp.NulledString())
			if runStuff(tp) {
				break
			}
		}
		return nil
	})
}


// RunConditions runs conditions for a file
func RunConditions(errLog *log.Logger, runStuff func(point *TimePoint) bool, conditionsPath string, loopFirstDay bool) {
	errLog.Printf("running conditions file: %s\n", conditionsPath)
	if filepath.Ext(conditionsPath) == ".xlsx" {
		if loopFirstDay {
			loopFromXlsx(errLog, runStuff, conditionsPath)
		}
		runFromXlsx(errLog, runStuff, conditionsPath)
	} else {
		errLog.Fatal("only .xlsx files are supported")
	}
}
