package chambertools

import "math"

// EnvironmentalStats is a struct that contains the environmental values we calculate from temperature and relative
// humidity
type EnvironmentalStats struct {
	VapourPressureDeficit   float64
	SaturatedVapourPressure float64
	ActualVapourPressure    float64
	//MixingRatio float64
	//SaturatedMixingRatio float64
	AbsoluteHumidity float64 //(in kg/m³)
}


// GetEnvironmentalStats calculates the environmental values from temperature in celsius and relative humidity as a
// percentage
func GetEnvironmentalStats(temperatureCelsius, relativeHumidity float64) (values *EnvironmentalStats) {
	values = new(EnvironmentalStats)
	// saturated vapor pressure
	values.SaturatedVapourPressure = 0.6108 * math.Exp(17.27*temperatureCelsius/(temperatureCelsius+237.3))
	// actual vapor pressure
	values.ActualVapourPressure = relativeHumidity / 100 * values.SaturatedVapourPressure
	// mixing ratio
	// values.mixingRatio = 621.97 * values.ActualVapourPressure / ((pressure64/10) - values.ActualVapourPressure)
	// saturated mixing ratio
	//values.SaturatedMixingRatio = 621.97 * values.SaturatedVapourPressure / ((pressure64/10) - values.SaturatedVapourPressure)
	// absolute humidity (in kg/m³)
	values.AbsoluteHumidity = (values.ActualVapourPressure / (461.5 * (temperatureCelsius + 273.15))) * 1000

	// this equation returns a negative value (in kPa), which while technically correct,
	// is invalid in this case because we are talking about a deficit.
	values.VapourPressureDeficit = (values.ActualVapourPressure - values.SaturatedVapourPressure) * -1
	return
}
